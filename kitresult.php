<?php
error_reporting(E_ALL);
$time = date('d.m.Y H:i:s');
function logging($msg, $logfile = 'kitlog.txt')
{
		file_put_contents($logfile, $msg.PHP_EOL, FILE_APPEND);
}
$random = rand(1,8849);
$picsDir = './pics';

if(!is_dir($picsDir)){
	mkdir("$picsDir", 0700);
}

$kitlerPath = "$picsDir/kitler$random.jpg";

function showKitler($logmsg)
{
	global $kitlerPath;
	logging("$logmsg");
	echo 'Do you see? There he is! <br/> <IMG src="/kitler/'.$kitlerPath.'">';
}

$url = "http://www.catsthatlooklikehitler.com/kitler/$kitlerPath";

if(!file_exists("$kitlerPath") || ($kitFileSize == 288)){
	copy("$url", "$kitlerPath");
	$kitFileSize = filesize($kitlerPath);
	showKitler("$time".PHP_EOL."File $kitlerPath was successfully created and it`s size is $kitFileSize bytes");
}elseif(file_exists("$kitlerPath")){
	showKitler("$time".PHP_EOL."File $kitlerPath is already existed");
}

$handle = curl_init($url);
curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
$response = curl_exec($handle);
$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
if($httpCode == 408) {
    echo "Request timeout, try to reload page or check your internet connection";
}

curl_close($handle);